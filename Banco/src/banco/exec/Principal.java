package banco.exec;

import banco.model.Imprimivel;
import banco.model.ContaBancaria;
import banco.model.ContaPoupanca;
import banco.model.ContaCorrente;
import banco.model.Relatorio;

    public class Principal {
        
	public static void main(String Agrs[]){
            
            ContaBancaria cb1 = new ContaCorrente(147,"2000-1",0.01);
            ContaBancaria cb2 = new ContaPoupanca("159-2",1000);
		
            cb1.sacar(100);
            cb2.sacar(6000);
            
            Relatorio obj1 = new Relatorio();
            obj1.gerarRelatorio((Imprimivel) cb1);
            obj1.gerarRelatorio((Imprimivel) cb2);
		
	}
}

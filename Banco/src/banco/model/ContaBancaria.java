package banco.model;

public abstract class ContaBancaria{
    
    private double saldo;
    private String numConta;

    public double getSaldo() {
        return saldo;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    public ContaBancaria(double saldo, String numConta) {
        this.saldo = saldo;
        this.numConta = numConta;
    }
    
    public void transferir(double valor, ContaBancaria beneficiario){
     
        this.sacar(valor);
        beneficiario.depositar(valor);
    }
    
    public abstract void sacar(double valor);
    public abstract void depositar(double valor);
    
    public void mostrarDados(){
        System.out.println("Conta: " + this.getNumConta() + "\nSaldo: " + this.getSaldo());
    }
    
}
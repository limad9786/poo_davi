package banco.model;
public class ContaPoupanca extends ContaBancaria implements Imprimivel{

    private double limite;

    public double getLimite() {
        return limite;
    }

    public void setLimite(double limite) {
        this.limite = limite;
    }

    public ContaPoupanca(String numConta, double saldo) {
        super(saldo, numConta);
        this.limite = saldo * 2;
    }
    
    public void sacar(double valor){
        if(this.getSaldo() >= valor){
            this.setSaldo((int) (this.getSaldo() - valor));
            System.out.println("Sua operação foi realizada com sucesso!");
        }
        else if(this.limite >= valor){
            double auxilia;
            this.limite = this.limite - valor;
            auxilia = this.getSaldo() - valor;
            System.out.println("Seu uso de limite = " + auxilia);
        }
        else {
            System.out.println("Ultrapassou ou estorou o limite do Bancario!");
        }
    }
    @Override
    public void depositar(double valor){
        if(valor >= 0){
            this.setSaldo((int) (this.getSaldo()+ valor));
            this.limite = this.getSaldo()*2;
        }
        else {
            System.out.println("A entrada é invalida!");
        }
    }
    @Override
    public void mostrarDados(){
        System.out.println("Sua conta poupança com limite: " + this.limite + ", Número dá conta: " + this.getNumConta() + ", Saldo: " + this.getSaldo());
    }
    
}
package banco.model;
public class ContaCorrente extends ContaBancaria implements Imprimivel{

    private double taxaDeOperacao;

    public double getTaxaDeOperacao() {
        return taxaDeOperacao;
    }

    public void setTaxaDeOperacao(double taxaDeOperacao) {
        this.taxaDeOperacao = taxaDeOperacao;
    }

    public ContaCorrente(double saldo,String numConta, double taxaDeOperacao){
        super(saldo, numConta);
        this.taxaDeOperacao = taxaDeOperacao;
    }
    
    public void sacar(double valor){
        if(this.getSaldo() >= valor+this.taxaDeOperacao){
            this.setSaldo((int) (this.getSaldo() - (valor+this.taxaDeOperacao)));
        }
        else{
            System.out.println("Saldo insuficiente!");
        }
    }
    @Override
    public void depositar(double valor){
        if(valor >= 0){
            this.setSaldo((int) ((this.getSaldo() + valor) - this.taxaDeOperacao));
        }
        else{
            System.out.println("Entrada Inválida");
        }
    }
    @Override
    public void mostrarDados(){
        System.out.println("Conta corrente com a taxa de operação: " + this.taxaDeOperacao + ", Conta: " + this.getNumConta() + ", Saldo: " + this.getSaldo());
    }
}

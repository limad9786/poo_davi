package banco.model;

import java.util.ArrayList;

public class Banco implements Imprimivel{

    private ArrayList<ContaBancaria> contas = new ArrayList <>();        

    void inserir(ContaBancaria conta){
        contas.add(conta);
    }
    void excluir(ContaBancaria conta){
        contas.remove(conta);
    }
    ContaBancaria procurarConta(String conta){
        for(ContaBancaria auxilia : contas){
            if(auxilia.getNumConta().equals(conta)){
            } 
            else {
                return auxilia;
            }
        }
        return null;
    }
    
 @Override
 public void mostrarDados(){
     for(ContaBancaria conta : contas){
         System.out.println(conta.getNumConta());
     }
  }
    
}

package cinema.si.quixada.model;

import java.util.Date;
import java.text.SimpleDateFormat;
public class EntradaDeCinema {
    
    private String tituloDoFilme;
    private String horario;
    private int sala;
    private int poltrona;
    private float valorOriginal;
    private boolean disponivel;

    public String getTituloDoFilme() {
        return tituloDoFilme;
    }

    public String getHorario() {
        return horario;
    }

    public int getSala() {
        return sala;
    }

    public int getPoltrona() {
        return poltrona;
    }

    public float getValorOriginal() {
        return valorOriginal;
    }

    public boolean isDisponivel() {
        return disponivel;
    }

    public EntradaDeCinema(String tituloDoFilme, String horario, int sala, int poltrona, float valorOriginal, boolean disponivel) {
        this.tituloDoFilme = tituloDoFilme;
        this.horario = horario;
        this.sala = sala;
        this.poltrona = poltrona;
        this.valorOriginal = valorOriginal;
        this.disponivel = disponivel;
    }
    
    public float calcularValorDeDesconto(int dia, int mes, int ano){
        Date dataAtual = new Date();
        
        SimpleDateFormat day = new SimpleDateFormat("dd");
        SimpleDateFormat mounth = new SimpleDateFormat("mm");
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        
        int diaAtual = Integer.parseInt(day.format(dataAtual));
        int mesAtual = Integer.parseInt(mounth.format(dataAtual));
        int anoAtual = Integer.parseInt(year.format(dataAtual));
        
        if(anoAtual - ano > 12){
            return this.valorOriginal;
        }
        else if(anoAtual - ano == 12){
            if(mesAtual > mes){
                return this.valorOriginal;
            }
            else if(mesAtual == mes){
                if(diaAtual >= dia){
                    return this.valorOriginal;
                }
                else{
                    return this.valorOriginal/2;
                }
            }
            else{
                return this.valorOriginal/2;
            }
        }
        else{
            return this.valorOriginal/2;
        }
    }
    public float calcularValorDeDesconto(int dia, int mes, int ano, int carteira){
        Date dataAtual = new Date();
        
        SimpleDateFormat year = new SimpleDateFormat("yyyy");
        int anoAtual = Integer.parseInt(year.format(dataAtual));
        
        if((anoAtual - ano) >= 12 && (anoAtual - ano) <= 15){
            return (float)(this.valorOriginal*0.6);
        }
        else if((anoAtual - ano) >= 16 && (anoAtual - ano) <= 20){
            return (float)(this.valorOriginal*0.7);
        }
        else{
            return (float)(this.valorOriginal*0.8);
        }
    }
    
    public void realizaVenda(){
        if(this.disponivel){
            this.disponivel = false;
        }
    }
    
    public String toString(){
        return "Entrada do Cinema: Titulo do filme: " + tituloDoFilme +
                ", Horario: " + horario + ", Sala: " + sala + ", Poltrona: " + poltrona +
                ", Valor Original: " + valorOriginal + ", Disponível: " + disponivel;
    }
}
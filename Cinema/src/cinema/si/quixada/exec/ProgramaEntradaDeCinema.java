package cinema.si.quixada.exec;

import cinema.si.quixada.model.EntradaDeCinema;

public class ProgramaEntradaDeCinema {
    
    
    public static void main(String[]args){
        
        EntradaDeCinema A = new EntradaDeCinema("Vingadores", "15:00", 1, 1, (float)18.00, true);
        EntradaDeCinema B = new EntradaDeCinema("Vingadores", "15:00", 1, 2, (float)18.00, true);
        EntradaDeCinema C = new EntradaDeCinema("Vingadores", "15:00", 1, 3, (float)18.00, true);
        
        System.out.println(A);
        System.out.println(B);
        System.out.println(C);
        
        float valorA = A.calcularValorDeDesconto(30, 10, 2007);
        float valorB = B.calcularValorDeDesconto(15, 10, 1998, 4200);
        float valorC = C.calcularValorDeDesconto(04 , 10, 1997, 8177);
        
        System.out.println("Valor do Ingresso: " + valorA);
        System.out.println("Valor do Ingresso: " + valorB);
        System.out.println("Valor do Ingresso: " + valorC);
        
        A.realizaVenda();
        B.realizaVenda();
        C.realizaVenda();
        
        System.out.println(A);
        System.out.println(B);
        System.out.println(C);
    }
}

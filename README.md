![Ufc Quixada - POO](https://www.quixada.ufc.br/wp-content/uploads/2017/10/logo.png)
# Comentarios da AP1 - POO #
- Cliente: faltou construtor vazio, função de calcular desconto sempre retorna 0, faltou toString. 
- Corrida: construtor com parâmetros não tem o atributo precoCorrida, não tem construtor vazio, não tem toString, a função de calcular o valor da corrida não foi feita a lógica. 
- MotoTaxi: usar o this.nomeAtributo para acessar atributos da própria classe, o método get é só para acesso de fora da classe por padrão. 
- Principal: não chamou todos os métodos implementados na projeto. 
- Pacotes: Usar os nomes padrões dos pacotes visto na disciplina, br.ufc.quixada.sigladoseucurso. 

teste
package ExercicioHerançaNewton.exec;


import ExercicioHerençaNewton.model.Administrador;
import ExercicioHerençaNewton.model.Empregado;
import ExercicioHerençaNewton.model.Fornecedor;
import ExercicioHerençaNewton.model.Operario;
import ExercicioHerençaNewton.model.Pessoa;
import ExercicioHerençaNewton.model.Vendedor;

public class Principal {

    public static void main(String[]args){
 
        Pessoa p = new Pessoa("Dawidh", "rua de número 0", "99678-8922");
        Pessoa p2 = new Pessoa("Kurotaka", "rua de número 1", "99652-5562");
        Pessoa p3 = new Pessoa("Meliodas777", "rua de número 2", "99678-8736");
        Pessoa p4 = new Pessoa("Gye Akemi", "rua de némero 3", "99615-8725");
        Pessoa p5 = new Pessoa("Gui", "rua de número 4", "99678-6523");
        
        p.toString();
        p2.toString();
        p3.toString();
        p4.toString();
        p5.toString();
        
        System.out.println(p);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
        System.out.println(p5);
        
        System.out.println("-------------------------------------------------------------------------------------------");
        
        Fornecedor f = new Fornecedor(2000, 1235.57f, "Dawidh", "rua de número 0", "99678-8922");
            System.out.println("Valor do saldo do fornecedor(a): " + f.getNome() + "\né equivalente: " + f.obterSaldo());
        System.out.println("-------------------------------------------------------------------------------------------");
        Empregado e = new Empregado(33, 2300, 1534.58f, "Kurotaka", "rua de número 1", "99652-5562");
            System.out.println("O valor do salário do empregado(a): " + e.getNome() + "\nO código: " + e.getCodigoSetor() + 
                                "\nÉ equivalente: " + e.calcularSalario());
        System.out.println("-------------------------------------------------------------------------------------------");    
        Administrador a = new Administrador(87, 1700, 0.5f, 2500, "Meliodas777", "rua de número 2", "99678-8736");
            System.out.println("O valor do salário do administrador(a): " + a.getNome() + "\nO código: " + a.getCodigoSetor() + 
                                "\nÉ equivalente: " + a.calcularSalario());
        System.out.println("-------------------------------------------------------------------------------------------");    
        Operario o = new Operario(25, 1800, 0.5f, 1500, 0.8f, "Gye Akemi", "rua de némero 3", "99615-8725");
            System.out.println("O valor do salário do operario(a): " + o.getNome() + "\nO código: " + o.getCodigoSetor() + 
                                "\nÉ equivalente: " + o.calcularSalario());
        System.out.println("-------------------------------------------------------------------------------------------");    
        Vendedor v = new Vendedor(88, 1700, 0.5f, 1900, 0.9f, "Gui", "rua de número 4", "99678-6523");
            System.out.println("O valor do salário do vendedor(a): " + v.getNome() + "\nO código: " + o.getCodigoSetor() + 
                                "\nÉ equivalente: " + o.calcularSalario());
        System.out.println("-------------------------------------------------------------------------------------------");    
    }

    
}

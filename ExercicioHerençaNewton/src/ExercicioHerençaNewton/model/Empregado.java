package ExercicioHerençaNewton.model;
public class Empregado extends Pessoa{

    private float codigoSetor;
    private float salarioBase;
    private float imposto;

    public float getCodigoSetor() {
        return codigoSetor;
    }

    public float getSalarioBase() {
        return salarioBase;
    }

    public float getImposto() {
        return imposto;
    }

    public void setCodigoSetor(float codigoSetor) {
        this.codigoSetor = codigoSetor;
    }

    public void setSalarioBase(float salarioBase) {
        this.salarioBase = salarioBase;
    }

    public void setImposto(float imposto) {
        this.imposto = imposto;
    }

    public Empregado(float codigoSetor, float salarioBase, float imposto, String nome, String endereco, String telefone) {
        super(nome, endereco, telefone);
        this.codigoSetor = codigoSetor;
        this.salarioBase = salarioBase;
        this.imposto = imposto;
    }
    
    public Empregado(){
        
    }
    
    public float calcularSalario(){
        
        return this.salarioBase - (this.salarioBase * this.imposto);
        
    }

    
}

package ExercicioHerençaNewton.model;
public class Administrador extends Empregado{

    private float ajudaDeCusto;

    public float getAjudaDeCusto() {
        return ajudaDeCusto;
    }

    public void setAjudaDeCusto(float ajudaDeCusto) {
        this.ajudaDeCusto = ajudaDeCusto;
    }
    
    
    public Administrador(float ajudaDeCusto, float codigoSetor, float salarioBase, float imposto, String nome, String endereco, String telefone) {
        super(codigoSetor, salarioBase, imposto, nome, endereco, telefone);
        this.ajudaDeCusto = ajudaDeCusto;
    }
    
    public float calcularSalario(){
        return super.calcularSalario() + this.ajudaDeCusto;
    }
    
    
    
}

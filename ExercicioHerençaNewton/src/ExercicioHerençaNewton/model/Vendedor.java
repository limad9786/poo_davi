package ExercicioHerençaNewton.model;
public class Vendedor extends Empregado{

    private float valorVendas;
    private float comissao;

    public float getValorVendas() {
        return valorVendas;
    }

    public float getComissao() {
        return comissao;
    }

    public void setValorVendas(float valorVendas) {
        this.valorVendas = valorVendas;
    }

    public void setComissao(float comissao) {
        this.comissao = comissao;
    }

    
    
    public Vendedor(float valorVendas, float comissao,float codigoSetor, float salarioBase, float imposto, String nome, String endereco, String telefone) {
        super(codigoSetor, salarioBase, imposto, nome, endereco, telefone);
        this.valorVendas = valorVendas;
        this.comissao = comissao;
    }
    
    public Vendedor(){
        
    }
    
    public float calcularSalario(){
        return super.calcularSalario() + (this.valorVendas * this.comissao);
    }
    
}

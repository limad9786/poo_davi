package ExercicioHerençaNewton.model;
public class Operario extends Empregado{
    
    private float valorProducao;
    private float comisao;

    public float getValorProducao() {
        return valorProducao;
    }

    public float getComisao() {
        return comisao;
    }

    public void setValorProducao(float valorProducao) {
        this.valorProducao = valorProducao;
    }

    public void setComisao(float comisao) {
        this.comisao = comisao;
    }

    public Operario(float valorProducao, float comisao, float codigoSetor, float salarioBase, float imposto, String nome, String endereco, String telefone) {
        super(codigoSetor, salarioBase, imposto, nome, endereco, telefone);
        this.valorProducao = valorProducao;
        this.comisao = comisao;
    }
    
    public Operario(){
    
    }
    
    public float calcularSalario(){
        return super.calcularSalario() + (this.valorProducao * this.comisao);
    }
    
    
}

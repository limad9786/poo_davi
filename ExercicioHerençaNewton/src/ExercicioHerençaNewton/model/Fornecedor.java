package ExercicioHerençaNewton.model;
public class Fornecedor extends Pessoa{

    private int valorCredito;
    private float valorDivida;

    public int getValorCredito() {
        return valorCredito;
    }

    public float getValorDivida() {
        return valorDivida;
    }

    public void setValorCredito(int valorCredito) {
        this.valorCredito = valorCredito;
    }

    public void setValorDivida(float valorDivida) {
        this.valorDivida = valorDivida;
    }

    public Fornecedor(int valorCredito, float valorDivida, String nome, String endereco, String telefone) {
        super(nome, endereco, telefone);
        this.valorCredito = valorCredito;
        this.valorDivida = valorDivida;
    }
    
    public Fornecedor(){
        
    }

    public float obterSaldo(){
        return this.valorCredito - this.valorDivida;
    }
     
}

package MotoTaxi.exec;

import MotoTaxi.model.MotoTaxi;
import MotoTaxi.model.Cliente;
import MotoTaxi.model.Corrida;
public class Principal {
    
       public static void main(String[]args){
            MotoTaxi p1 = new MotoTaxi("João", "8888", "9999", 9.5f);
            MotoTaxi p2 = new MotoTaxi("Lucas", "8888", "8888", 8.8f);
            
            System.out.println(p1);
            System.out.println(p2);
            
            
            Cliente c1 = new Cliente("Savio", "8989898", "Estudante");
            Cliente c2 = new Cliente("Vitor", "8989898", "Policial");
            Cliente c3 = new Cliente("Breno", "8989898", "Professor");
            
            System.out.println(c1);
            System.out.println(c2);
            System.out.println(c3);
            
            Corrida t1 = new Corrida("Praça", "UFC", 50.0f, 12.00f);
            Corrida t2 = new Corrida("Hotel", "Ifce", 50.0f, 12.00f);
            Corrida t3 = new Corrida("Parque", "pinheiro", 50.0f, 12.00f);
            
            System.out.println(t1);
            System.out.println(t2);
            System.out.println(t3);
            
            float corrida1 = c1.solicitardesconto("Estudante");
            float corrida2 = c2.solicitardesconto("Policial");
            float corrida3 = c3.solicitardesconto("professor");
            
            System.out.println("Valor da corrida: " + corrida1);
            System.out.println("Valor da corrida: " + corrida2);
            System.out.println("Valor da corrida: " + corrida3);
            
            
    }    
}

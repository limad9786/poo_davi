package MotoTaxi.model;


public class MotoTaxi {

    private String nome;
    private String cnh;
    private String placa;
    private float nota;

    public MotoTaxi(String nome, String cnh, String placa, float nota) {
        this.nome = nome;
        this.cnh = cnh;
        this.placa = placa;
        this.nota = nota;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setNota(float nota) {
        this.nota = nota;
    }

    public String getNome() {
        return nome;
    }

    public String getCnh() {
        return cnh;
    }

    public String getPlaca() {
        return placa;
    }

    public float getNota() {
        return nota;
    }
    
    public void relizarCorrida(Cliente cliente,Corrida corrida){
        System.out.println("Nome do cliente: " + cliente.getNome());
        System.out.println("Nome do moto taxi: " + getNome());
        System.out.println("Nota do moto taxi: " + getNota());
        System.out.println("Partida: " + corrida.getPartida());
        System.out.println("Destino: " + corrida.getDestino());
    }
    
    public String toString(){
             return "Nome: " + this.getNome() + 
                     "Cnh: " + this.getCnh() +
                     "Placa: " + this.getPlaca() +
                     "Nota: " +this.getNota();
    }
}

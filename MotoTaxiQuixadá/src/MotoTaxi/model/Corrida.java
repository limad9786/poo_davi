package MotoTaxi.model;
public class Corrida {

    private String partida;
    private String destino;
    private float precoKm;
    private float precoCorrida;

    public Corrida(String partida, String destino, float precoKm, float precoCorrida) {
        this.partida = partida;
        this.destino = destino;
        this.precoKm = precoKm;
        this.precoCorrida = precoCorrida;
    }

    public String getPartida() {
        return partida;
    }

    public String getDestino() {
        return destino;
    }

    public float getPrecoKm() {
        return precoKm;
    }

    public float getPrecoCorrida() {
        return precoCorrida;
    }

    public void setPartida(String partida) {
        this.partida = partida;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public void setPrecoKm(float precoKm) {
        this.precoKm = precoKm;
    }

    public void setPrecoCorrida(float precoCorrida) {
        this.precoCorrida = precoCorrida;
    }
    
    public float calcularValorCorrida(int distancia){
        return 0;
        
    }
}

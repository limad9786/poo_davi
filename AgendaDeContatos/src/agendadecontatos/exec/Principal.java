package agendadecontatos.exec;

import agendadecontatos.model.Circulo;
import agendadecontatos.model.PessoaJuridica;
import agendadecontatos.model.PessoaFisica;
import agendadecontatos.model.Retangulo;
import agendadecontatos.model.Agenda;
import agendadecontatos.model.Quadrado;
import java.util.Scanner;

public class Principal {
    public static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
       
       PessoaFisica pf = new PessoaFisica("08053230318", "Victor", "Rua de número 17", "13/03/2000", "Solteiro\n");
       PessoaJuridica pj = new PessoaJuridica("Anny", "Rua de número 15", "19/04/2000","0215151515", "799456", "Anaju\n");
       PessoaFisica pf2 = new PessoaFisica("052145789002", "Davi", "Rua de número 0", "30/11/1999", "Casado\n");
       
       Agenda a = new Agenda();
   
       a.addPessoa(pf);
       a.addPessoa(pj);
       a.addPessoa(pf2);
       
       System.out.println("DESORDENADA");
       System.out.println("-------------------------------");
       a.imprimeAgenda();
       a.ordenaVetor();
       System.out.println("\n");
       System.out.println("ORDENADA");
       System.out.println("-------------------------------");
       a.imprimeAgenda();
       System.out.print("\n");
       a.excluirPessoa();
       a.imprimeAgenda();
       System.out.print("\n");
       a.buscarPessoa();
       
       Circulo c = new Circulo(12, "Preto", false);
       Retangulo r = new Retangulo(6, 3, "Azul", true);
       Quadrado q = new Quadrado(10, 5, "Verde", true);
       
       System.out.println("A área do círculo: " + c.retornarArea());
       System.out.println("Perímetro do círculo: " + c.retornarPerimetro()); 
       System.out.println("Área do retângulo: " + r.retornarArea());
       System.out.println("perímetro do retângulo: " + r.retornarPerimetro());
       System.out.println("Área do quadrado: " + q.retornarArea());
       System.out.println("Perímetro do quadrado: " + q.retornarPerimetro());
    }
}

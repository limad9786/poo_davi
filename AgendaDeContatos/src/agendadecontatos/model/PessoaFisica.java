package agendadecontatos.model;

public class PessoaFisica extends Pessoa{
    private String cpf;
    private String estadoCivil;
    
    public PessoaFisica(String cpf, String nome, String endereco,String dataNasc, String estadoCivil){
        super(nome, endereco, dataNasc);
        this.cpf = cpf;
        this.estadoCivil = estadoCivil;
    }
    
    public PessoaFisica(){
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    
    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String toString() {
        super.imprimirPessoa();
        return "Cpf: " + this.cpf + "\nEstado Civil: " + this.estadoCivil;
    }
}

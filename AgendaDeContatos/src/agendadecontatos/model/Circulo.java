package agendadecontatos.model;

public class Circulo extends FiguraGeometrica{
    
    private double raio;
    private String cor;
    private boolean filled;
    
    public Circulo(double raio, String cor, boolean filled){
        this.raio = raio;
        this.cor = cor;
        this.filled = filled;
    }
    
    @Override
    public double retornarArea(){
        
        return 3.14 * (this.raio * this.raio);
        
    }
    
    @Override
    public double retornarPerimetro(){
        
        return (2 * 3.14) * this.raio;
        
    }
}

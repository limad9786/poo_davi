	package agendadecontatos.model;

public class PessoaJuridica extends Pessoa{
    
    private String cnpj;
    private String inscricaoEstadual;
    private String razaoSocial;
    
    public PessoaJuridica(String nome, String endereco, String dataNasc,String cnpj, String inscricaoEstadual,String razaoSocial){
        super(nome, endereco, dataNasc);
        
        this.cnpj = cnpj;
        this.inscricaoEstadual = inscricaoEstadual;
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getInscricaoEstadual() {
        return inscricaoEstadual;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    public String toString(){
        super.imprimirPessoa();
        return "Cnpj: " + this.cnpj + "\nInscrição Estadual: " + this.inscricaoEstadual+ "\nRazão Social: " + this.razaoSocial;
    }
}

package agendadecontatos.model;

public class Quadrado extends FiguraGeometrica{
    private double base;
    private double altura;
    private String cor;
    private boolean filled;
    
    public Quadrado(double base, double altura, String cor, boolean filled){
        
        this.base = base;
        this.altura = altura;
        this.cor = cor;
        this.filled = filled;
    }
    
    @Override
    public double retornarArea(){
        
        return this.base * this.altura;
        
    }
    
    @Override
    public double retornarPerimetro(){
        
        return (2 * this.base) + (2 * this.altura);
        
    }
}

package agendadecontatos.model;

public abstract class FiguraGeometrica {
    
    public abstract double retornarArea();
    
    public abstract double retornarPerimetro();
}

package agendadecontatos.model;

public class Pessoa {
    private String nome;
    private String endereco;
    private String dataNasc;
    
    public Pessoa(String nome, String endereco, String dataNasc){
        this.nome = nome;
        this.endereco = endereco;
        this.dataNasc = dataNasc;
    }
    
    public Pessoa(){
    
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getdataNasc() {
        return dataNasc;
    }

    public void setdataNasc(String dataNasc) {
        this.dataNasc = dataNasc;
    }
    
    public void imprimirPessoa(){
        System.out.println("Nome: " + this.nome);
        System.out.println("Endereço: " + this.endereco);
        System.out.println("Data de nascimento: " + this.dataNasc);
    }
}

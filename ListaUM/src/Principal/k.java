package Principal;
import java.util.Scanner;
public class k {
    public static Scanner scan = new Scanner(System.in);
    
    public static void main(String[]args){
        System.out.println("Digite dois números:");
        int num1 = scan.nextInt();
        int num2 = scan.nextInt();
        
        System.out.println(Cor(num1, num2));
        
    }
    public static String Cor(int num1, int num2){
        if(num1%2 == 0 && num2%2 == 0){
            return "Azul";
        }
        else if(num1%2 != 0 && num2%2 != 0){
            return "Vermelho";
        }
        else{
            return "Amarelo";
        }
    }
}
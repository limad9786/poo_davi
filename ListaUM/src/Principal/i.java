package Principal;
import java.util.Scanner;
public class i {
    public static Scanner scan = new Scanner(System.in);
    
    public static void main(String[]args){
        Menu();
        
    }
    public static void Menu(){
        System.out.println("Digite dois números:");
        int x = scan.nextInt();
        int y = scan.nextInt();
        System.out.println("Digite um operação Matemática:: ");
        System.out.println("(Adição(+), (Subtração(-)), (Multiplicação(*)) ou (Divisão(/))");
        
        String calculo = scan.next();
        int result = Calculadora(x,y,calculo);
        
        if(result == -999){
            System.out.println("Inválido");
        }else{
            System.out.println("Resultado: "+result);
        }
    } 
    public static int Calculadora(int num1, int num2, String ope){
        
        switch(ope){
            case "+":
                return num1 + num2;
            case "-":
                return num1 - num2;
            case "*":
                return num1 * num2;
            case "/":
                return num1 / num2;
            default:
                return -999;
        }
    }
}

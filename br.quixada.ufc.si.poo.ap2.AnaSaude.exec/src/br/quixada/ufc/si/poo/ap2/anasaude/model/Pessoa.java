package br.quixada.ufc.si.poo.ap2.anasaude.model;
public abstract class Pessoa {

    private String nome;
    
    public Pessoa(){
        
    }

    public Pessoa(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome=" + nome + '}';
    }
    
}

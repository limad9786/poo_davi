package br.quixada.ufc.si.poo.ap2.anasaude.model;
public class Analista extends Funcionario{

    public  Analista(){
        super();
    }
    
    public void procesarContrato(Operadora op, Contrato c){
        op.cadastrarContratoCliente(c);
        daBonificacao();
        
    }

  
    public Analista(String cpf, String matricula, float salario, String nome) {
        super(cpf, matricula, salario, nome);
    }

    @Override
    public void daBonificacao() {
           float novoSalario = this.getSalario() + (this.getSalario() * 0.02f);
           this.setSalario(novoSalario);
   }

   @Override
    public String toString() {
        return "Analista{" + '}';
    }  
    
}

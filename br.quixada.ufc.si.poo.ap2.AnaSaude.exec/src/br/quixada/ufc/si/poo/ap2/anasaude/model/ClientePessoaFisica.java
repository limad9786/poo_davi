package br.quixada.ufc.si.poo.ap2.anasaude.model;

import java.time.LocalDate;

public class ClientePessoaFisica extends Cliente{

    private String cpf;
    private LocalDate dNasc;
    
    public ClientePessoaFisica(){
        super();
    }

    public ClientePessoaFisica(String cpf, LocalDate dNasc, String endereco, String nome) {
        super(endereco, nome);
        this.cpf = cpf;
        this.dNasc = dNasc;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getdNasc() {
        return dNasc;
    }

    public void setdNasc(LocalDate dNasc) {
        this.dNasc = dNasc;
    }

    @Override
    public String toString() {
        return "ClientePessoaFisica{" + "cpf=" + cpf + ", dNasc=" + dNasc + '}';
    }
    
    
    
}

package br.quixada.ufc.si.poo.ap2.anasaude.exec;


import br.quixada.ufc.si.poo.ap2.anasaude.model.Analista;
import br.quixada.ufc.si.poo.ap2.anasaude.model.ClientePessoaFisica;
import br.quixada.ufc.si.poo.ap2.anasaude.model.ClientePessoaJuridica;
import br.quixada.ufc.si.poo.ap2.anasaude.model.Contrato;
import br.quixada.ufc.si.poo.ap2.anasaude.model.Operadora;
import br.quixada.ufc.si.poo.ap2.anasaude.model.Vendedor;
import java.time.LocalDate;
import java.time.Month;

public class PrincipalImprimivel {

    public static void main(String[]args){
        
        LocalDate dNasc = LocalDate.of(2000, Month.MARCH, 13);
        ClientePessoaFisica cpf1 = new ClientePessoaFisica("40025236", dNasc, "Davi", "Rua de número 0");
        
        LocalDate dNasc2 = LocalDate.of(1999, Month.MARCH, 20);
        ClientePessoaFisica cpf2 = new ClientePessoaFisica("400233155", dNasc2, "Diego", "Rua de número 1");
        
        LocalDate dNasc3 = LocalDate.of(2000, Month.MARCH, 14);
        ClientePessoaFisica cpf3 = new ClientePessoaFisica("4000656463", dNasc3, "Breno", "Rua de némero 2");
        
        LocalDate dAbertura = LocalDate.of(2000, Month.MARCH, 13);
        ClientePessoaJuridica cpj1 = new ClientePessoaJuridica("12454000", dAbertura, "Juvenal", "Rua de número 3"); 
        
        Vendedor v1 = new Vendedor("121214545", "54542121154", 1.500f, "Matheus");
        Vendedor v2 = new Vendedor("2211212121", "5457878787", 1.600f, "Letícia");
        Vendedor v3 = new Vendedor("565989865632", "78745896", 1.100f, "Ohara");
        
        Operadora op = new Operadora();
        
        LocalDate dInicio = LocalDate.of(2000, Month.MARCH, 14);
        Contrato c1 = new Contrato(212121, cpf1, v1, dInicio, 1.400f);
        LocalDate dInicio2 = LocalDate.of(2001, Month.MARCH, 15);
        Contrato c2 = new Contrato(5151515, cpf2, v2, dInicio2, 1.250f);
        LocalDate dInicio3 = LocalDate.of(2002, Month.MARCH, 30);
        Contrato c3 = new Contrato(21212121, cpj1, v3, dInicio2, 1.450f);
        
        Analista a1 = new Analista("2121455212", "54545454", 1.500f, "Garen");
        
        
        op.setCodOperadora(500);
        op.setNome("Ahri");
        a1.procesarContrato(op, c1);
        a1.procesarContrato(op, c2);
        a1.procesarContrato(op, c3);
        System.out.println(op);
        
        
        
    }
    
}

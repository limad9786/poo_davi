package br.quixada.ufc.si.poo.ap2.anasaude.model;

import java.time.LocalDate;

public class ClientePessoaJuridica extends Cliente{

    private String cnpj;
    private LocalDate dAbertura;
    
    public ClientePessoaJuridica(){
        super();
    }

    public ClientePessoaJuridica(String cnpj, LocalDate dAbertura, String endereco, String nome) {
        super(endereco, nome);
        this.cnpj = cnpj;
        this.dAbertura = dAbertura;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public LocalDate getdAbertura() {
        return dAbertura;
    }

    public void setdAbertura(LocalDate dAbertura) {
        this.dAbertura = dAbertura;
    }

    @Override
    public String toString() {
        return "ClientePessoaJuridica{" + "cnpj=" + cnpj + ", dAbertura=" + dAbertura + '}';
    }
    
    
}
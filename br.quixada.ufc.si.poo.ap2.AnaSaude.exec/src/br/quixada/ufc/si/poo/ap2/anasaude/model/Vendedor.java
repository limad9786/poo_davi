package br.quixada.ufc.si.poo.ap2.anasaude.model;
public class Vendedor extends Funcionario{

    public Vendedor(){
        super();
    }
    
    public void realizarVenda(float valorContrato){
         float novoSalario = this.getSalario() + (valorContrato * 0.5f);
         this.setSalario(novoSalario);
         daBonificacao();
    }

    public Vendedor(String cpf, String matricula, float salario, String nome) {
        super(cpf, matricula, salario, nome);
    }    

    @Override
    public void daBonificacao() {
        float novoSalario = this.getSalario() + (this.getSalario() * 0.05f);
        this.setSalario(novoSalario);
    }

    @Override
    public String toString() {
        return "Vendedor{" + '}';
    }

    
    
    
}

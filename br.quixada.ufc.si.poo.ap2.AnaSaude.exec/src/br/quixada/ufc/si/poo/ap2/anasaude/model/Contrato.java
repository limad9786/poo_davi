package br.quixada.ufc.si.poo.ap2.anasaude.model;

import java.time.LocalDate;

public class Contrato {

    private int codContrato;
    private Cliente cliente;
    private Funcionario vendedor;
    private LocalDate dInicio;
    private float valorContrato;
    
    public  Contrato(){
        super();
    }

    public Contrato(int codContrato, Cliente cliente, Funcionario vendedor, LocalDate dInicio, float valorContrato) {
        this.codContrato = codContrato;
        this.cliente = cliente;
        this.vendedor = vendedor;
        this.dInicio = dInicio;
        this.valorContrato = valorContrato;
    }

    public int getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(int codContrato) {
        this.codContrato = codContrato;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Funcionario getVendedor() {
        return vendedor;
    }

    public void setVendedor(Funcionario vendedor) {
        this.vendedor = vendedor;
    }

    public LocalDate getdInicio() {
        return dInicio;
    }

    public void setdInicio(LocalDate dInicio) {
        this.dInicio = dInicio;
    }

    public float getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(float valorContrato) {
        this.valorContrato = valorContrato;
    }

    @Override
    public String toString() {
        return "Contrato{" + "codContrato=" + codContrato + ", cliente=" + cliente + ", \nvendedor=" + vendedor + ", dInicio=" + dInicio + ", valorContrato=" + valorContrato + '}';
    }
    
    
    
}

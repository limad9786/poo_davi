package br.quixada.ufc.si.poo.ap2.anasaude.model;
public abstract class Cliente extends Pessoa{

    private String endereco;
    
    public Cliente(){
        
    }

    public Cliente(String endereco, String nome) {
        super(nome);
        this.endereco = endereco;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return "Cliente{" + "endereco=" + endereco + "\n" +'}';
    }
    
}
